// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDpJ2bid-0FqS1FrK05fKC8THhXssALY0M",
    authDomain: "online-store-7dcc8.firebaseapp.com",
    databaseURL: "https://online-store-7dcc8.firebaseio.com",
    projectId: "online-store-7dcc8",
    storageBucket: "online-store-7dcc8.appspot.com",
    messagingSenderId: "727454328638"
  },
  nodeBackendURL: "https://online-store-backend.herokuapp.com"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
