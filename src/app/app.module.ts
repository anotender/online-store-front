import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ProductsComponent} from './products/products.component';
import {ProductComponent} from './product/product.component';
import {CartComponent} from './cart/cart.component';
import {ProductDetailsComponent} from './product-details/product-details.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NavbarComponent} from './navbar/navbar.component';
import {Ng5SliderModule} from "ng5-slider";
import {NgSelectModule} from "@ng-select/ng-select";
import {CategoryFilterPipe} from "./pipe/category-filter.pipe";
import {MinPriceFilterPipe} from './pipe/min-price-filter.pipe';
import {MaxPriceFilterPipe} from "./pipe/max-price-filter.pipe";
import {TextFilterPipe} from "./pipe/text-filter.pipe";
import {OrderComponent} from './order/order.component';
import {NgxPaginationModule} from "ngx-pagination";
import {OrdersComponent} from './orders/orders.component';
import {OrderDetailsComponent} from './order-details/order-details.component';
import {environment} from "../environments/environment.prod";
import {AngularFireModule} from "@angular/fire";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AuthComponent} from './auth/auth.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import {ConfigurationComponent} from './configuration/configuration.component';
import {HttpClientModule} from "@angular/common/http";
import {OrderStatusFilterPipe} from './pipe/order-status-filter.pipe';
import { PromoComponent } from './promo/promo.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductComponent,
    CartComponent,
    ProductDetailsComponent,
    NavbarComponent,
    CategoryFilterPipe,
    MinPriceFilterPipe,
    MaxPriceFilterPipe,
    TextFilterPipe,
    OrderStatusFilterPipe,
    OrderComponent,
    OrdersComponent,
    OrderDetailsComponent,
    AuthComponent,
    ConfigurationComponent,
    PromoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    Ng5SliderModule,
    NgSelectModule,
    NgxPaginationModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-left',
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
