import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AngularFireAuth} from "@angular/fire/auth";
import {map} from "rxjs/operators";
import {AngularFirestore} from "@angular/fire/firestore";
import UserCredential = firebase.auth.UserCredential;

export interface Credentials {
  email: string;
  password: string;
}

export interface UserRole {
  userId: string,
  role: string
}

@Injectable({providedIn: 'root'})
export class AuthService {

  private currentUserRole: UserRole = null;

  constructor(private fireAuth: AngularFireAuth, private db: AngularFirestore) {
    this.currentUserRole = this.getCurrentUserRoleFromLocalStorage();
  }

  hasAnyRole(roles: string[]): boolean {
    return this.currentUserRole !== null && roles.includes(this.currentUserRole.role);
  }

  isAuthenticated(): Observable<boolean> {
    return this.fireAuth.authState.pipe(map(state => state !== null));
  }

  getCurrentUserId(): string {
    return localStorage.getItem('userId');
  }

  login({email, password}: Credentials): Promise<UserCredential> {
    return this.fireAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(userCredential => {
        localStorage.setItem('userId', userCredential.user.uid);
        this.db
          .collection<UserRole>('users', ref => ref.where('userId', '==', userCredential.user.uid).limit(1))
          .valueChanges()
          .subscribe(userRoles => {
            if (!userRoles || userRoles.length === 0) {
              throw Error('UserRole not found!')
            }
            this.currentUserRole = userRoles[0];
            localStorage.setItem('currentUserRole', this.currentUserRole.role);
          }, failure => {
            console.error(failure);
            this.currentUserRole = null;
            localStorage.setItem('currentUserRole', null);
            this.logout();
          });
        return userCredential;
      });
  }

  register({email, password}: Credentials): Promise<UserCredential> {
    return this.fireAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(userCredential => {
        this.db.collection('users').add({
          userId: userCredential.user.uid,
          role: 'user'
        }).then(console.log, console.error);
        return userCredential;
      });
  }

  logout(): Promise<void> {
    this.currentUserRole = null;
    localStorage.setItem('userId', null);
    localStorage.setItem('currentUserRole', null);
    return this.fireAuth.auth.signOut();
  }

  private getCurrentUserRoleFromLocalStorage(): UserRole {
    let userId = localStorage.getItem('userId');
    let role = localStorage.getItem('currentUserRole');
    if (userId && role) {
      return this.currentUserRole = {
        userId: userId,
        role: role
      }
    }
    return null;
  }
}
