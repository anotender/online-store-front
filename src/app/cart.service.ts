import {Injectable} from '@angular/core';
import {Product} from "./model/product.model";
import {CartEntry} from "./model/cart-entry.model";
import {OrderItem} from "./model/order-item.model";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private authService: AuthService) {
  }

  getCartContent(): CartEntry[] {
    return this.getProductsInCartFromLocalStorage();
  }

  getOrderItemsFromCart(): OrderItem[] {
    return this.getProductsInCartFromLocalStorage().map(this.cartEntryToOrderItem);
  }

  addProductToCart(product: Product): void {
    if (product.numberOfProducts < 1) {
      return;
    }

    let productsInCart: CartEntry[] = this.getCartContent();
    const index: number = productsInCart.findIndex(cartEntry => cartEntry.product.id === product.id);
    if (index > -1) {
      productsInCart[index].numberOfOccurrences++;
    } else {
      productsInCart.push(new CartEntry(product, 1));
    }
    this.saveProductsInCartInLocalStorage(productsInCart);
  }

  removeFromCart(productId: string): void {
    let productsInCart: CartEntry[] = this.getCartContent();
    let index = productsInCart.findIndex(cartEntry => cartEntry.product.id === productId);
    if (index < 0) {
      return;
    }

    if (productsInCart[index].numberOfOccurrences === 1) {
      productsInCart.splice(index, 1);
    } else {
      productsInCart[index].numberOfOccurrences--;
    }
    this.saveProductsInCartInLocalStorage(productsInCart);
  }

  getNumberOfProductsInCart(): number {
    return this.getCartContent()
      .map(cartEntry => cartEntry.numberOfOccurrences)
      .reduce((a, b) => a + b, 0);
  }

  getCartTotalValue(): number {
    let cartContent = this.getCartContent();
    return cartContent
      .map(cartEntry => cartEntry.product.price * cartEntry.numberOfOccurrences)
      .reduce((a, b) => a + b, 0);
  }

  clearCart(): void {
    this.saveProductsInCartInLocalStorage([]);
  }

  private cartEntryToOrderItem(cartEntry: CartEntry): OrderItem {
    return {
      productId: cartEntry.product.id,
      numberOfOccurrences: cartEntry.numberOfOccurrences,
      collected: false
    };
  }

  private saveProductsInCartInLocalStorage(cartEntries: CartEntry[]): void {
    let currentUserId = this.authService.getCurrentUserId();
    localStorage.setItem('productsInCart_' + currentUserId, JSON.stringify(cartEntries));
  }

  private getProductsInCartFromLocalStorage(): CartEntry[] {
    let currentUserId = this.authService.getCurrentUserId();
    let productsInCart = JSON.parse(localStorage.getItem('productsInCart_' + currentUserId));
    if (productsInCart) {
      return productsInCart;
    }
    return [];
  }

}
