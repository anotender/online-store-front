import {Component, OnInit} from '@angular/core';
import {Order} from "../model/order.model";
import {ToastrService} from "ngx-toastr";
import {ServiceFactory} from "../service.factory";
import {OrderUtils} from "../utils/order.utils";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  public orders: Order[] = [];

  private selectedOrder: Order = null;

  private orderStatuses: string[] = ['COMPLETED', 'IN PROGRESS', 'WAITING'];

  private selectedStatuses: string[] = [];

  private isOrderCompleted = OrderUtils.isOrderCompleted;

  private isOrderInProgress = OrderUtils.isOrderInProgress;

  private getOrderStatus = OrderUtils.getOrderStatus;

  constructor(private serviceFactory: ServiceFactory,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.serviceFactory
      .getOrderService()
      .getOrders()
      .subscribe(orders => {
        this.orders = orders;
      }, failure => {
        console.error(failure);
        this.toastr.error('Failed to fetch orders')
      });
  }

  private getFormattedOrderStatus(order: Order): string {
    let orderStatus = OrderUtils.getOrderStatus(order);
    if (orderStatus === 'COMPLETED') {
      return orderStatus + ' (' + new Date(order.completionDate).toLocaleString() + ')';
    }
    return orderStatus;
  }

  private onSelectOrder(order: Order): void {
    this.selectedOrder = order;
  }

}
