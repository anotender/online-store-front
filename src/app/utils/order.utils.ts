import {Order} from "../model/order.model";

export class OrderUtils {

  public static getOrderStatus(order: Order): string {
    if (this.isOrderCompleted(order)) {
      return 'COMPLETED';
    } else if (this.isOrderInProgress(order)) {
      return 'IN PROGRESS';
    }
    return 'WAITING';
  }

  public static isOrderInProgress(order: Order): boolean {
    return !order.completionDate && order.items.some(item => item.collected);
  }

  public static isOrderCompleted(order: Order): boolean {
    return !!order.completionDate;
  }

}
