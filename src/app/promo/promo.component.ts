import {Component, OnInit} from '@angular/core';
import {ServiceFactory} from "../service.factory";
import {Product} from "../model/product.model";
import {PromoService} from "../promo.service";
import {ConfigurationService} from "../configuration.service";

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.css']
})
export class PromoComponent implements OnInit {

  public products: Product[];

  public selectedProduct: Product = null;

  public percentage: number = 0;

  public duration: number = 0;

  public backendType: string = '';

  constructor(private serviceFactory: ServiceFactory,
              private promoService: PromoService,
              private configurationService: ConfigurationService) {
  }

  ngOnInit() {
    this.serviceFactory
      .getProductService()
      .getProducts()
      .subscribe(products => this.products = products);
    this.configurationService
      .getBackendType()
      .subscribe(backendType => this.backendType = backendType);
  }

  public onSubmit(): void {
    this.promoService.addPromo({
      productId: this.selectedProduct.id,
      percentage: this.percentage,
      endDate: new Date().getTime() + this.duration * 60000
    });
  }


}
