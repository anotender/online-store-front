import {Injectable} from '@angular/core';
import {Product} from "./model/product.model";
import {AngularFirestore} from "@angular/fire/firestore";
import {first, map, switchMap, tap} from "rxjs/operators";
import {Observable} from "rxjs";
import {Order} from "./model/order.model";
import {ProductService} from "./product-service.interface";
import {fromPromise} from "rxjs/internal-compatibility";

@Injectable({
  providedIn: 'root'
})
export class FirestoreProductService implements ProductService {

  constructor(private db: AngularFirestore) {
  }

  getProduct(productId: string): Observable<Product> {
    return this.db
      .doc<Product>('products/' + productId)
      .valueChanges()
      .pipe(tap(product => product.id = productId));
  }

  getProducts(): Observable<Product[]> {
    return this.db
      .collection('products')
      .snapshotChanges()
      .pipe(map(actions => {
        return actions.map(a => {
          const product: Product = a.payload.doc.data() as Product;
          product.id = a.payload.doc.id;
          return product;
        });
      }));
  }

  getProductsByIds(ids: string[]): Observable<Product[]> {
    return this.getProducts()
      .pipe(map(products => products.filter(product => ids.includes(product.id))));
  }

  addProduct(product: Product): Observable<any> {
    return fromPromise(this.db.collection('products').add({...product}));
  }

  deleteProduct(productId: string): Observable<any> {
    return this.db.collection<Order>('orders')
      .valueChanges()
      .pipe(first())
      .pipe(map(orders => orders.filter(order => order.completionDate === null)))
      .pipe(tap(orders => {
        if (this.isProductInSomeOrder(productId, orders)) {
          throw new Error('Product is in incomplete order');
        }
      }))
      .pipe(switchMap(() => this.db.doc('products/' + productId).delete()));
  }

  updateProduct(productId: string, product: Product): Observable<any> {
    return fromPromise(this.db.doc('products/' + productId).update({...product}));
  }

  private isProductInSomeOrder(productId: string, orders: Order[]): boolean {
    return orders.some(order => order.items.some(orderItem => orderItem.productId === productId));
  }
}
