import {Component} from '@angular/core';
import {CartService} from "../cart.service";
import {CartEntry} from "../model/cart-entry.model";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {

  constructor(private cartService: CartService) {
  }

  getProductsInCart(): CartEntry[] {
    return this.cartService.getCartContent();
  }

  removeFromCart(productId: string): void {
    this.cartService.removeFromCart(productId);
  }

  getNumberOfProductsInCart(): number {
    return this.cartService.getNumberOfProductsInCart()
  }

  getCartTotalValue(): number {
    return this.cartService.getCartTotalValue();
  }

}
