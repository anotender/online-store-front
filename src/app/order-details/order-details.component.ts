import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../model/order.model";
import {OrderItem} from "../model/order-item.model";
import {Product} from "../model/product.model";
import {ToastrService} from "ngx-toastr";
import {ServiceFactory} from "../service.factory";

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  @Input()
  public order: Order;

  private products: Product[] = [];

  constructor(private serviceFactory: ServiceFactory,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.serviceFactory
      .getProductService()
      .getProductsByIds(this.order.items.map(item => item.productId))
      .subscribe(products => this.products = products);
  }

  public getProductName(productId: string): string {
    let index: number = this.products.findIndex(product => product.id === productId);
    if (index < 0) {
      return '';
    }
    return this.products[index].name;
  }

  public collectAllOrderItems(): void {
    this.order.items.forEach(orderItem => orderItem.collected = true);
    this.updateOrder();
  }

  public collectOrderItem(orderItem: OrderItem): void {
    orderItem.collected = true;
    this.updateOrder();
  }

  public completeOrder(): void {
    this.order.completionDate = new Date().getTime();
    this.updateOrder();
  }

  public areAllOrderItemsCollected(): boolean {
    return this.order.items
      .every(orderItem => orderItem.collected);
  }

  private updateOrder(): void {
    this.serviceFactory
      .getOrderService()
      .updateOrder(this.order.id, this.order)
      .subscribe(() => {
        this.toastr.success('Order updated');
      }, failure => {
        console.error(failure);
        this.toastr.error('Failed to update order');
      });
  }

}
