import {Component, OnInit} from '@angular/core';
import {ConfigurationService} from "../configuration.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {

  public availableBackendTypes: string[] = [];

  public backendType: string;

  constructor(private configurationService: ConfigurationService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.availableBackendTypes = ConfigurationService.getAvailableBackendTypes();
    this.configurationService
      .getBackendType()
      .subscribe(backendType => this.backendType = backendType);
  }

  public save(): void {
    this.configurationService
      .updateBackendType(this.backendType)
      .then(() => {
        this.toastr.success('Backend type updated');
      }, failure => {
        console.error(failure);
        this.toastr.error('Failed to update backend type');
      })
  }

}
