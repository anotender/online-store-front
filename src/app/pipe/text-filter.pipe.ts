import {Pipe, PipeTransform} from '@angular/core';
import {Product} from "../model/product.model";

@Pipe({
  name: 'textFilter'
})
export class TextFilterPipe implements PipeTransform {

  transform(products: Product[], text: string): Product[] {
    if (!products || products.length === 0 || !text || text.length < 3) {
      return products;
    }
    return products.filter(p => this.containsIgnoreCase(p.name, text) || this.containsIgnoreCase(p.description, text));
  }

  private containsIgnoreCase(s1: string, s2: string): boolean {
    return s1.toLowerCase().includes(s2.toLowerCase());
  }

}
