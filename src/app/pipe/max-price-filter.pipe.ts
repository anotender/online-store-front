import {Pipe, PipeTransform} from '@angular/core';
import {Product} from "../model/product.model";

@Pipe({
  name: 'maxPriceFilter'
})
export class MaxPriceFilterPipe implements PipeTransform {

  transform(products: Product[], maxPrice: number): Product[] {
    if (!products || products.length === 0) {
      return products;
    }
    return products.filter(p => p.price <= maxPrice);
  }

}
