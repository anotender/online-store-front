import {Pipe, PipeTransform} from '@angular/core';
import {Product} from "../model/product.model";

@Pipe({
  name: 'minPriceFilter'
})
export class MinPriceFilterPipe implements PipeTransform {

  transform(products: Product[], minPrice: number): Product[] {
    if (!products || products.length === 0) {
      return products;
    }
    return products.filter(p => p.price >= minPrice);
  }

}
