import {Pipe, PipeTransform} from '@angular/core';
import {Order} from "../model/order.model";
import {OrderUtils} from "../utils/order.utils";

@Pipe({
  name: 'orderStatusFilter'
})
export class OrderStatusFilterPipe implements PipeTransform {

  transform(orders: Order[], orderStatuses: string[]): any {
    if (!orders || orders.length === 0 || !orderStatuses || orderStatuses.length === 0) {
      return orders;
    }
    return orders.filter(order => orderStatuses.includes(OrderUtils.getOrderStatus(order)));
  }

}
