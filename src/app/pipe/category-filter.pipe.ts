import {Pipe, PipeTransform} from '@angular/core';
import {Product} from "../model/product.model";

@Pipe({
  name: 'categoryFilter'
})
export class CategoryFilterPipe implements PipeTransform {

  transform(products: Product[], categories: string[]): Product[] {
    if (this.isArrayEmpty(products) || this.isArrayEmpty(categories)) {
      return products;
    }
    return products
      .filter(p => p.categories.some(c => categories.includes(c)));
  }

  private isArrayEmpty(array: any[]): boolean {
    return !array || array.length === 0;
  }
}
