import {Injectable} from '@angular/core';
import {Order} from "./model/order.model";
import {AngularFirestore} from "@angular/fire/firestore";
import {first, map, switchMap, tap} from "rxjs/operators";
import {forkJoin, Observable} from "rxjs";
import {FirestoreProductService} from "./firestore-product.service";
import {OrderItem} from "./model/order-item.model";
import {OrderService} from "./order-service.interface";
import {fromPromise} from "rxjs/internal-compatibility";

@Injectable({
  providedIn: 'root'
})
export class FirestoreOrderService implements OrderService {

  constructor(private db: AngularFirestore,
              private productService: FirestoreProductService) {
  }

  addOrder(order: Order): Observable<any> {
    let productIds: string[] = order.items.map(orderItem => orderItem.productId);
    return this.productService
      .getProductsByIds(productIds)
      .pipe(first())
      .pipe(tap(products => {
        if (products.length !== productIds.length) {
          throw new Error('Product missing');
        }
      }))
      .pipe(map(products => products.map(product => {
        const orderItem: OrderItem = order.items.find(orderItem => orderItem.productId === product.id);
        if (product.numberOfProducts < orderItem.numberOfOccurrences) {
          throw new Error('Not enough products to create order');
        }
        product.numberOfProducts -= orderItem.numberOfOccurrences;
        return this.productService.updateProduct(product.id, product);
      })))
      .pipe(map(productUpdates => forkJoin(productUpdates)))
      .pipe(switchMap(() => this.db.collection('orders').add({...order})));
  }

  updateOrder(orderId: string, order: Order): Observable<any> {
    return fromPromise(this.db.doc('orders/' + orderId).update(order));
  }

  getOrders(): Observable<Order[]> {
    return this.db
      .collection('orders')
      .snapshotChanges()
      .pipe(map(actions => actions.map(a => {
        const order: Order = a.payload.doc.data() as Order;
        order.id = a.payload.doc.id;
        return order;
      })));
  }

}
