import {Injectable} from '@angular/core';
import {Order} from "./model/order.model";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {OrderService} from "./order-service.interface";
import {tap} from "rxjs/operators";
import {environment} from "../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class NodeOrderService implements OrderService {

  private static ORDERS_URL = environment.nodeBackendURL + '/orders';

  constructor(private http: HttpClient) {
  }

  addOrder(order: Order): Observable<any> {
    return this.http.post(NodeOrderService.ORDERS_URL, order);
  }

  updateOrder(orderId: string, order: Order): Observable<any> {
    return this.http.put(NodeOrderService.ORDERS_URL + '/' + orderId, order);
  }

  getOrders(): Observable<Order[]> {
    return this.http
      .get<any[]>(NodeOrderService.ORDERS_URL)
      .pipe(tap(orders => orders.forEach(order => order.id = order._id)));
  }

}
