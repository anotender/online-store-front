import {Injectable} from "@angular/core";
import {FirestoreProductService} from "./firestore-product.service";
import {ProductService} from "./product-service.interface";
import {FirestoreOrderService} from "./firestore-order.service";
import {OrderService} from "./order-service.interface";
import {NodeProductService} from "./node-product.service";
import {NodeOrderService} from "./node-order.service";
import {ConfigurationService} from "./configuration.service";

@Injectable({
  providedIn: 'root'
})
export class ServiceFactory {

  private backendType: string = 'FIRESTORE';
  private productServices: Map<string, ProductService> = new Map<string, ProductService>();
  private orderServices: Map<string, OrderService> = new Map<string, OrderService>();

  constructor(private firestoreProductService: FirestoreProductService,
              private nodeProductService: NodeProductService,
              private firestoreOrderService: FirestoreOrderService,
              private nodeOderService: NodeOrderService,
              private configurationService: ConfigurationService) {
    this.productServices.set('FIRESTORE', this.firestoreProductService);
    this.productServices.set('NODE', this.nodeProductService);
    this.orderServices.set('FIRESTORE', this.firestoreOrderService);
    this.orderServices.set('NODE', this.nodeOderService);
    this.configurationService.getBackendType()
      .subscribe(backendType => this.backendType = backendType);
  }

  getProductService(): ProductService {
    return this.productServices.get(this.backendType);
  }

  getOrderService(): OrderService {
    return this.orderServices.get(this.backendType);
  }

}
