export class OrderItem {
  productId: string;
  numberOfOccurrences: number;
  collected: boolean;
}
