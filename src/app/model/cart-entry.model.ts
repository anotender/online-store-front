import {Product} from "./product.model";

export class CartEntry {
  product: Product;
  numberOfOccurrences: number;

  constructor(product: Product, numberOfOccurrences: number) {
    this.product = product;
    this.numberOfOccurrences = numberOfOccurrences;
  }
}
