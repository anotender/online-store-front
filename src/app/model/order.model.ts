import {OrderItem} from "./order-item.model";

export class Order {
  id: string;
  recipient: string;
  address: string;
  completionDate: number;
  totalValue: number;
  items: OrderItem[];
}
