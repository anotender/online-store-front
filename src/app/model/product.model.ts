export class Product {
  id: string;
  name: string;
  numberOfProducts: number;
  price: number;
  description: string;
  imageUrl: string;
  categories: string[];
}
