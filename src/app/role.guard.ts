import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let expectedRoles: string[] = next.data.expectedRoles;

    let hasExpectedRole = this.authService.hasAnyRole(expectedRoles);
    if (!hasExpectedRole) {
      this.router.navigateByUrl('/products');
    }
    return hasExpectedRole;
  }

}
