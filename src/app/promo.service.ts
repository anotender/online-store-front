import {Injectable} from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {fromPromise} from "rxjs/internal-compatibility";

export class Promo {
  productId: string;
  percentage: number;
  endDate: number;
}

@Injectable({
  providedIn: 'root'
})
export class PromoService {

  constructor(private db: AngularFirestore) {
  }

  getPromos(): Observable<Promo[]> {
    return this.db.collection<Promo>('promos').valueChanges();
  }

  addPromo(promo: Promo): Observable<any> {
    return fromPromise(this.db.collection('promos').add({...promo}));
  }
}
