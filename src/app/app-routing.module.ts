import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductsComponent} from "./products/products.component";
import {CartComponent} from "./cart/cart.component";
import {ProductDetailsComponent} from "./product-details/product-details.component";
import {OrderComponent} from "./order/order.component";
import {OrdersComponent} from "./orders/orders.component";
import {AuthComponent} from "./auth/auth.component";
import {AuthGuard} from "./auth.guard";
import {RoleGuard} from "./role.guard";
import {ConfigurationComponent} from "./configuration/configuration.component";
import {PromoComponent} from "./promo/promo.component";

const routes: Routes = [
  {
    path: 'auth',
    component: AuthComponent
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'new-order',
    component: OrderComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orders',
    component: OrdersComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['admin', 'employee']
    }
  },
  {
    path: 'promo',
    component: PromoComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['admin', 'employee']
    }
  },
  {
    path: 'products/:id',
    component: ProductDetailsComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['admin']
    }
  },
  {
    path: 'products',
    component: ProductsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'configuration',
    component: ConfigurationComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {
      expectedRoles: ['admin']
    }
  },
  {
    path: '**',
    component: ProductsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
