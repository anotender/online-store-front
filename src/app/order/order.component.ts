import {Component, OnInit} from '@angular/core';
import {CartService} from "../cart.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {Order} from "../model/order.model";
import {ToastrService} from "ngx-toastr";
import {ServiceFactory} from "../service.factory";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  public orderForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private serviceFactory: ServiceFactory,
              private cartService: CartService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.orderForm = this.fb.group({
      recipient: ['', Validators.required],
      address: ['', Validators.required]
    });
  }

  onSubmit(): void {
    if (this.orderForm.invalid) {
      return;
    }

    this.serviceFactory
      .getOrderService()
      .addOrder(this.prepareOrder())
      .subscribe(() => {
        this.cartService.clearCart();
        this.toastr.success('Order completed');
        this.router.navigateByUrl('products');
      }, failure => {
        console.error(failure);
        this.toastr.error('Failed to create order');
      });
  }

  private prepareOrder(): Order {
    const order: Order = new Order();

    order.recipient = this.orderForm.controls['recipient'].value;
    order.address = this.orderForm.controls['address'].value;
    order.completionDate = null;
    order.totalValue = this.cartService.getCartTotalValue();
    order.items = this.cartService.getOrderItemsFromCart();

    return order;
  }

}
