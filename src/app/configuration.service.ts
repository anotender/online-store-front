import {Injectable} from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

interface BackendConfiguration {
  type: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(private db: AngularFirestore) {
  }

  static getAvailableBackendTypes(): string[] {
    return ['FIRESTORE', 'NODE'];
  }

  getBackendType(): Observable<string> {
    return this.db.doc<BackendConfiguration>('configuration/backend')
      .valueChanges()
      .pipe(map(backendConfiguration => backendConfiguration.type));
  }

  updateBackendType(backendType: string): Promise<void> {
    return this.db.doc('configuration/backend').update({type: backendType});
  }
}
