import {Observable} from "rxjs";
import {DocumentReference} from "@angular/fire/firestore";
import {Order} from "./model/order.model";

export abstract class OrderService {

  abstract addOrder(order: Order): Observable<any>;

  abstract updateOrder(orderId: string, order: Order): Observable<any>;

  abstract getOrders(): Observable<Order[]>;

}
