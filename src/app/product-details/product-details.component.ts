import {Component, OnInit} from '@angular/core';
import {Product} from "../model/product.model";
import {ToastrService} from "ngx-toastr";
import {ActivatedRoute, Router} from "@angular/router";
import {filter, map, switchMap} from "rxjs/operators";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ServiceFactory} from "../service.factory";

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  public productForm: FormGroup;

  private productId: string = '0';

  constructor(private serviceFactory: ServiceFactory,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.productForm = this.fb.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      numberOfProducts: ['', Validators.required],
      imageUrl: ['', Validators.required],
      categories: ['', Validators.required],
      description: ['']
    });
    this.route.paramMap
      .pipe(map(params => params.get('id')))
      .pipe(filter(this.isNotNewProduct))
      .pipe(switchMap(id => this.serviceFactory.getProductService().getProduct(id)))
      .subscribe(product => {
        this.productId = product.id;
        this.setProductInForm(product);
      }, failure => {
        console.error(failure);
        this.router.navigateByUrl('/products');
        this.toastr.error('Failed to fetch product');
      })
  }

  submit(): void {
    if (this.isNotNewProduct(this.productId)) {
      this.updateProduct();
    } else {
      this.addProduct();
    }
  }

  reset(): void {
    this.productForm.reset();
  }

  private addProduct(): void {
    this.serviceFactory
      .getProductService()
      .addProduct(this.getProductFromForm())
      .subscribe(() => {
        this.reset();
        this.router.navigateByUrl('/products');
        this.toastr.success('New product added');
      }, failure => {
        console.error(failure);
        this.toastr.error('Failed to add product');
      });
  }

  private updateProduct(): void {
    this.serviceFactory
      .getProductService()
      .updateProduct(this.productId, this.getProductFromForm())
      .subscribe(() => {
        this.reset();
        this.router.navigateByUrl('/products');
        this.toastr.success('Product updated');
      }, failure => {
        console.error(failure);
        this.toastr.error('Failed to update product');
      });
  }

  private isNotNewProduct(id: string): boolean {
    return id !== '0';
  }

  private setProductInForm(product: Product): void {
    this.productForm.controls['categories'].setValue(product.categories);
    this.productForm.controls['description'].setValue(product.description);
    this.productForm.controls['imageUrl'].setValue(product.imageUrl);
    this.productForm.controls['name'].setValue(product.name);
    this.productForm.controls['numberOfProducts'].setValue(product.numberOfProducts);
    this.productForm.controls['price'].setValue(product.price);
  }

  private getProductFromForm(): Product {
    const product: Product = new Product();

    product.categories = this.productForm.controls['categories'].value;
    product.description = this.productForm.controls['description'].value;
    product.imageUrl = this.productForm.controls['imageUrl'].value;
    product.name = this.productForm.controls['name'].value;
    product.numberOfProducts = this.productForm.controls['numberOfProducts'].value;
    product.price = this.productForm.controls['price'].value;

    return product;
  }

}
