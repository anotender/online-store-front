import {Injectable} from '@angular/core';
import {Product} from "./model/product.model";
import {Observable} from "rxjs";
import {ProductService} from "./product-service.interface";
import {HttpClient} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {environment} from "../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class NodeProductService implements ProductService {

  private static PRODUCTS_URL: string = environment.nodeBackendURL + '/products';

  constructor(private http: HttpClient) {
  }

  getProduct(productId: string): Observable<Product> {
    return this.http
      .get<any>(NodeProductService.PRODUCTS_URL + '/' + productId)
      .pipe(tap(product => product.id = productId));
  }

  getProducts(): Observable<Product[]> {
    return this.http
      .get<any[]>(NodeProductService.PRODUCTS_URL)
      .pipe(tap(products => products.forEach(product => product.id = product._id)));
  }

  getProductsByIds(ids: string[]): Observable<Product[]> {
    return this.getProducts()
      .pipe(map(products => products.filter(product => ids.includes(product.id))));
  }

  addProduct(product: Product): Observable<any> {
    return this.http.post(NodeProductService.PRODUCTS_URL, product);
  }

  deleteProduct(productId: string): Observable<any> {
    return this.http.delete(NodeProductService.PRODUCTS_URL + '/' + productId);
  }

  updateProduct(productId: string, product: Product): Observable<any> {
    return this.http.put(NodeProductService.PRODUCTS_URL + '/' + productId, product);
  }
}
