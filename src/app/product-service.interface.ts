import {Observable} from "rxjs";
import {Product} from "./model/product.model";
import {DocumentReference} from "@angular/fire/firestore";

export abstract class ProductService {

  abstract getProduct(productId: string): Observable<Product>;

  abstract getProducts(): Observable<Product[]>;

  abstract getProductsByIds(ids: string[]): Observable<Product[]>;

  abstract addProduct(product: Product): Observable<any>;

  abstract deleteProduct(productId: string): Observable<any>;

  abstract updateProduct(productId: string, product: Product): Observable<any>;

}
