import {Component, OnInit} from '@angular/core';
import {CartService} from "../cart.service";
import {AuthService, UserRole} from "../auth.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public authenticated: boolean;

  constructor(private cartService: CartService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.isAuthenticated().subscribe(authenticated => this.authenticated = authenticated);
  }

  isAdmin(): boolean {
    return this.authService.hasAnyRole(['admin']);
  }

  isAdminOrEmployee(): boolean {
    return this.authService.hasAnyRole(['admin', 'employee']);
  }

  getCartState(): string {
    return this.getNumberOfProductsInCart() + 'product(s), ' + this.getCartTotalValue() + ' PLN';
  }

  logout(): void {
    this.authService.logout().then(() => {
      this.authenticated = false;
    });
  }

  private getNumberOfProductsInCart(): number {
    return this.cartService.getNumberOfProductsInCart();
  }

  private getCartTotalValue(): number {
    return this.cartService.getCartTotalValue();
  }

}
