import {Component, OnInit} from '@angular/core';
import {Product} from "../model/product.model";
import {CartService} from "../cart.service";
import {Options} from "ng5-slider";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../auth.service";
import {ServiceFactory} from "../service.factory";
import {Promo, PromoService} from "../promo.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public minPrice: number = 0;
  public maxPrice: number = 100;
  public sliderOptions: Options = {};
  public availableCategories: string[] = [];
  public selectedCategories: string[] = [];
  public searchText: string;
  public currentPage: number = 1;
  public availableItemsPerPageValues: number[] = [5, 10, 20];
  public itemsPerPage: number = 5;
  public products: Product[] = [];
  private promos: Promo[] = [];
  private promoSubscription: Subscription;

  constructor(private serviceFactory: ServiceFactory,
              private promoService: PromoService,
              private cartService: CartService,
              private authService: AuthService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.serviceFactory
      .getProductService()
      .getProducts()
      .subscribe(products => {
        this.products = products;
        this.availableCategories = this.getCategories();
        this.minPrice = this.getMinPrice();
        this.maxPrice = this.getMaxPrice();
        this.sliderOptions = {
          floor: this.minPrice,
          ceil: this.maxPrice
        };
        if (!this.promoSubscription) {
          this.promoSubscription = this.promoService
            .getPromos()
            .subscribe(promos => {
              this.promos = promos.filter(promo => promo.endDate > new Date().getTime());
              if (this.promos.length !== 0) {
                this.toastr.success('There are new products with discount! Check them out!');
              }
            }, failure => {
              console.log(failure);
              this.toastr.error('Failed to fetch promos');
            });
        }
      }, failure => {
        console.error(failure);
        this.toastr.error('Failed to fetch products')
      });
  }

  handleDeleteProductEvent(): void {
    this.ngOnInit();
  }

  handlePageChangeEvent(newPage: number): void {
    this.currentPage = newPage;
  }

  isAdmin(): boolean {
    return this.authService.hasAnyRole(['admin']);
  }

  getPromoForProduct(product: Product): Promo {
    return this.promos.find(promo => promo.productId === product.id);
  }

  private getCategories(): string[] {
    let categories = this.products
      .map(p => p.categories)
      .reduce((previousValue, currentValue) => previousValue.concat(currentValue), []);
    return Array.from(new Set(categories)).sort();
  }

  private getMinPrice(): number {
    return this.products
      .sort((p1, p2) => p1.price - p2.price)[0].price;
  }

  private getMaxPrice(): number {
    return this.products
      .sort((p1, p2) => p2.price - p1.price)[0].price;
  }

}
