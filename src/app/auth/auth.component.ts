import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService, Credentials} from "../auth.service";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  public authForm: FormGroup;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.authForm = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', Validators.required)
    });
  }

  public onLogin(): void {
    this.authService.login(this.getCredentials()).then(() => {
      this.router.navigateByUrl('/products');
    }, failure => {
      console.error(failure);
      this.toastr.error('Wrong username or password');
    });
  }

  public onRegister(): void {
    this.authService.register(this.getCredentials()).then(() => {
      this.onLogin();
    }, failure => {
      console.error(failure);
      this.toastr.error('Cannot register')
    });
  }

  private getCredentials(): Credentials {
    return {
      email: this.authForm.controls['email'].value,
      password: this.authForm.controls['password'].value
    };
  }

}
