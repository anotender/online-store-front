import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from "../model/product.model";
import {CartService} from "../cart.service";
import {AuthService} from "../auth.service";
import {ToastrService} from "ngx-toastr";
import {ServiceFactory} from "../service.factory";
import {Promo} from "../promo.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {

  constructor(private serviceFactory: ServiceFactory,
              private cartService: CartService,
              private authService: AuthService,
              private toastr: ToastrService) {
  }

  @Input()
  public product: Product;

  @Input()
  public cheap: boolean = false;

  @Input()
  public expensive: boolean = false;

  @Input()
  public promo: Promo = null;

  @Output()
  public deleteProductEvent: EventEmitter<void> = new EventEmitter<void>();

  deleteProduct(): void {
    this.serviceFactory
      .getProductService()
      .deleteProduct(this.product.id)
      .subscribe(() => {
        this.deleteProductEvent.emit();
        this.toastr.success('Product deleted');
      }, failure => {
        console.error(failure);
        this.toastr.error(failure.toString(), 'Failed to remove product');
      });
  }

  addToCart(): void {
    let productCopy: Product = JSON.parse(JSON.stringify(this.product));
    productCopy.price = this.getProductPrice();
    this.cartService.addProductToCart(productCopy);
    this.toastr.success('Product added to cart');
  }

  isAdmin(): boolean {
    return this.authService.hasAnyRole(['admin']);
  }

  getProductPrice() {
    return this.hasPromo() ? this.product.price * (100 - this.promo.percentage) / 100 : this.product.price
  }

  hasPromo(): boolean {
    return this.promo && this.promo.endDate > new Date().getTime();
  }

}
